package com.stb



import org.junit.*
import grails.test.mixin.*

@TestFor(PersistentLoginController)
@Mock(PersistentLogin)
class PersistentLoginControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/persistentLogin/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.persistentLoginInstanceList.size() == 0
        assert model.persistentLoginInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.persistentLoginInstance != null
    }

    void testSave() {
        controller.save()

        assert model.persistentLoginInstance != null
        assert view == '/persistentLogin/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/persistentLogin/show/1'
        assert controller.flash.message != null
        assert PersistentLogin.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/persistentLogin/list'


        populateValidParams(params)
        def persistentLogin = new PersistentLogin(params)

        assert persistentLogin.save() != null

        params.id = persistentLogin.id

        def model = controller.show()

        assert model.persistentLoginInstance == persistentLogin
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/persistentLogin/list'


        populateValidParams(params)
        def persistentLogin = new PersistentLogin(params)

        assert persistentLogin.save() != null

        params.id = persistentLogin.id

        def model = controller.edit()

        assert model.persistentLoginInstance == persistentLogin
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/persistentLogin/list'

        response.reset()


        populateValidParams(params)
        def persistentLogin = new PersistentLogin(params)

        assert persistentLogin.save() != null

        // test invalid parameters in update
        params.id = persistentLogin.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/persistentLogin/edit"
        assert model.persistentLoginInstance != null

        persistentLogin.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/persistentLogin/show/$persistentLogin.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        persistentLogin.clearErrors()

        populateValidParams(params)
        params.id = persistentLogin.id
        params.version = -1
        controller.update()

        assert view == "/persistentLogin/edit"
        assert model.persistentLoginInstance != null
        assert model.persistentLoginInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/persistentLogin/list'

        response.reset()

        populateValidParams(params)
        def persistentLogin = new PersistentLogin(params)

        assert persistentLogin.save() != null
        assert PersistentLogin.count() == 1

        params.id = persistentLogin.id

        controller.delete()

        assert PersistentLogin.count() == 0
        assert PersistentLogin.get(persistentLogin.id) == null
        assert response.redirectedUrl == '/persistentLogin/list'
    }
}
