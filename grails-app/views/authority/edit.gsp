

<%@ page import="com.stb.Authority" %>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'authority.label', default: 'Authority')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>

<section id="edit" class="first">

	<g:hasErrors bean="${authorityInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${authorityInstance}" as="list" />
	</div>
	</g:hasErrors>

	<g:form method="post" class="form-horizontal" >
		<g:hiddenField name="id" value="${authorityInstance?.id}" />
		<g:hiddenField name="version" value="${authorityInstance?.version}" />
				
						<div class="control-group ${hasErrors(bean: authorityInstance, field: 'authority', 'error')}">
							<label for="authority"><g:message code="authority.authority.label" default="Authority" /></label>
							<div class="controls">
								<g:textField name="authority" value="${authorityInstance?.authority}" />
								<span class="help-inline">${hasErrors(bean: authorityInstance, field: 'authority', 'error')}</span>
							</div>
						</div>
				
		<div class="form-actions">
			<g:actionSubmit class="save btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
			<g:actionSubmit class="delete btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            <button class="btn" type="reset">Cancel</button>
		</div>
	</g:form>

</section>
			
</body>

</html>
