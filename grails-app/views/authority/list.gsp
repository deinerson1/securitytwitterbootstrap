
<%@ page import="com.stb.Authority" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'authority.label', default: 'Authority')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="id" title="${message(code: 'authority.id.label', default: 'Id')}" />
			
				<g:sortableColumn property="authority" title="${message(code: 'authority.authority.label', default: 'Authority')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${authorityInstanceList}" status="i" var="authorityInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${authorityInstance.id}">${fieldValue(bean: authorityInstance, field: "id")}</g:link></td>
			
				<td>${fieldValue(bean: authorityInstance, field: "authority")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${authorityInstanceTotal}" />
	</div>
</section>

</body>

</html>
