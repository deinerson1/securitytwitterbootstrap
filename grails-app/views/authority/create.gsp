

<%@ page import="com.stb.Authority" %>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'authority.label', default: 'Authority')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<section id="create" class="first">

	<g:hasErrors bean="${authorityInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${authorityInstance}" as="list" />
	</div>
	</g:hasErrors>
	
	<g:form action="save" class="form-horizontal" >
				
							<div class="control-group ${hasErrors(bean: authorityInstance, field: 'authority', 'error')}">
								<label for="authority" class="control-label"><g:message code="authority.authority.label" default="Authority" /></label>
				            	<div class="controls">
									<g:textField name="authority" value="${authorityInstance?.authority}" />
									<span class="help-inline">${hasErrors(bean: authorityInstance, field: 'authority', 'error')}</span>
								</div>
							</div>
				
		<div class="form-actions">
			<g:submitButton name="create" class="save btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
            <button class="btn" type="reset">Cancel</button>
		</div>
	</g:form>
	
</section>
		
</body>

</html>
