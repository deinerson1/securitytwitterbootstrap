
<%@ page import="com.stb.PersistentLogin" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'persistentLogin.label', default: 'PersistentLogin')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="id" title="${message(code: 'persistentLogin.id.label', default: 'Id')}" />
			
				<g:sortableColumn property="username" title="${message(code: 'persistentLogin.username.label', default: 'Username')}" />
			
				<g:sortableColumn property="token" title="${message(code: 'persistentLogin.token.label', default: 'Token')}" />
			
				<g:sortableColumn property="lastUsed" title="${message(code: 'persistentLogin.lastUsed.label', default: 'Last Used')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${persistentLoginInstanceList}" status="i" var="persistentLoginInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${persistentLoginInstance.id}">${fieldValue(bean: persistentLoginInstance, field: "id")}</g:link></td>
			
				<td>${fieldValue(bean: persistentLoginInstance, field: "username")}</td>
			
				<td>${fieldValue(bean: persistentLoginInstance, field: "token")}</td>
			
				<td><g:formatDate date="${persistentLoginInstance.lastUsed}" /></td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${persistentLoginInstanceTotal}" />
	</div>
</section>

</body>

</html>
