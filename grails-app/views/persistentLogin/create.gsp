

<%@ page import="com.stb.PersistentLogin" %>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'persistentLogin.label', default: 'PersistentLogin')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<section id="create" class="first">

	<g:hasErrors bean="${persistentLoginInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${persistentLoginInstance}" as="list" />
	</div>
	</g:hasErrors>
	
	<g:form action="save" class="form-horizontal" >
				
							<div class="control-group ${hasErrors(bean: persistentLoginInstance, field: 'username', 'error')}">
								<label for="username" class="control-label"><g:message code="persistentLogin.username.label" default="Username" /></label>
				            	<div class="controls">
									<g:textField name="username" maxlength="64" value="${persistentLoginInstance?.username}" />
									<span class="help-inline">${hasErrors(bean: persistentLoginInstance, field: 'username', 'error')}</span>
								</div>
							</div>
				
							<div class="control-group ${hasErrors(bean: persistentLoginInstance, field: 'token', 'error')}">
								<label for="token" class="control-label"><g:message code="persistentLogin.token.label" default="Token" /></label>
				            	<div class="controls">
									<g:textField name="token" maxlength="64" value="${persistentLoginInstance?.token}" />
									<span class="help-inline">${hasErrors(bean: persistentLoginInstance, field: 'token', 'error')}</span>
								</div>
							</div>
				
							<div class="control-group ${hasErrors(bean: persistentLoginInstance, field: 'lastUsed', 'error')}">
								<label for="lastUsed" class="control-label"><g:message code="persistentLogin.lastUsed.label" default="Last Used" /></label>
				            	<div class="controls">
									<g:datePicker name="lastUsed" precision="day" value="${persistentLoginInstance?.lastUsed}"  />
									<span class="help-inline">${hasErrors(bean: persistentLoginInstance, field: 'lastUsed', 'error')}</span>
								</div>
							</div>
				
		<div class="form-actions">
			<g:submitButton name="create" class="save btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
            <button class="btn" type="reset">Cancel</button>
		</div>
	</g:form>
	
</section>
		
</body>

</html>
