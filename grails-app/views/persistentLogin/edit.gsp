

<%@ page import="com.stb.PersistentLogin" %>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'persistentLogin.label', default: 'PersistentLogin')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>

<section id="edit" class="first">

	<g:hasErrors bean="${persistentLoginInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${persistentLoginInstance}" as="list" />
	</div>
	</g:hasErrors>

	<g:form method="post" class="form-horizontal" >
		<g:hiddenField name="id" value="${persistentLoginInstance?.id}" />
		<g:hiddenField name="version" value="${persistentLoginInstance?.version}" />
				
						<div class="control-group ${hasErrors(bean: persistentLoginInstance, field: 'username', 'error')}">
							<label for="username"><g:message code="persistentLogin.username.label" default="Username" /></label>
							<div class="controls">
								<g:textField name="username" maxlength="64" value="${persistentLoginInstance?.username}" />
								<span class="help-inline">${hasErrors(bean: persistentLoginInstance, field: 'username', 'error')}</span>
							</div>
						</div>
				
						<div class="control-group ${hasErrors(bean: persistentLoginInstance, field: 'token', 'error')}">
							<label for="token"><g:message code="persistentLogin.token.label" default="Token" /></label>
							<div class="controls">
								<g:textField name="token" maxlength="64" value="${persistentLoginInstance?.token}" />
								<span class="help-inline">${hasErrors(bean: persistentLoginInstance, field: 'token', 'error')}</span>
							</div>
						</div>
				
						<div class="control-group ${hasErrors(bean: persistentLoginInstance, field: 'lastUsed', 'error')}">
							<label for="lastUsed"><g:message code="persistentLogin.lastUsed.label" default="Last Used" /></label>
							<div class="controls">
								<g:datePicker name="lastUsed" precision="day" value="${persistentLoginInstance?.lastUsed}"  />
								<span class="help-inline">${hasErrors(bean: persistentLoginInstance, field: 'lastUsed', 'error')}</span>
							</div>
						</div>
				
		<div class="form-actions">
			<g:actionSubmit class="save btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
			<g:actionSubmit class="delete btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            <button class="btn" type="reset">Cancel</button>
		</div>
	</g:form>

</section>
			
</body>

</html>
