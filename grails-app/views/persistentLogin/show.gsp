
<%@ page import="com.stb.PersistentLogin" %>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'persistentLogin.label', default: 'PersistentLogin')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="persistentLogin.id.label" default="Id" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: persistentLoginInstance, field: "id")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="persistentLogin.username.label" default="Username" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: persistentLoginInstance, field: "username")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="persistentLogin.token.label" default="Token" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: persistentLoginInstance, field: "token")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="persistentLogin.lastUsed.label" default="Last Used" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${persistentLoginInstance?.lastUsed}" /></td>
				
			</tr>
		
		</tbody>
	</table>
	<div class="buttons">
		<g:form>
			<g:hiddenField name="id" value="${persistentLoginInstance?.id}" />
			<span class="button"><g:actionSubmit class="edit btn-primary" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
			<span class="button"><g:actionSubmit class="delete btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
		</g:form>
	</div>
</section>

</body>

</html>
