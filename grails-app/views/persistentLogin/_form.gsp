<%@ page import="com.stb.PersistentLogin" %>



<div class="fieldcontain ${hasErrors(bean: persistentLoginInstance, field: 'username', 'error')} ">
	<label for="username">
		<g:message code="persistentLogin.username.label" default="Username" />
		
	</label>
	<g:textField name="username" maxlength="64" value="${persistentLoginInstance?.username}" />
</div>

<div class="fieldcontain ${hasErrors(bean: persistentLoginInstance, field: 'token', 'error')} ">
	<label for="token">
		<g:message code="persistentLogin.token.label" default="Token" />
		
	</label>
	<g:textField name="token" maxlength="64" value="${persistentLoginInstance?.token}" />
</div>

<div class="fieldcontain ${hasErrors(bean: persistentLoginInstance, field: 'lastUsed', 'error')} required">
	<label for="lastUsed">
		<g:message code="persistentLogin.lastUsed.label" default="Last Used" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="lastUsed" precision="day" value="${persistentLoginInstance?.lastUsed}"  />
</div>

