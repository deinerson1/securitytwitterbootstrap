<html>

<head>
  <meta name='layout' content='kickstart'/>
  <title><g:message code='spring.security.ui.login.title'/></title>

</head>

<body>

<div class="modal" id="loginModal">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Login</h3>
  </div>
	<div class="modal-body">
	<form action='${postUrl}' method='POST' id="loginForm" name="loginForm" autocomplete='on'>
	<div class="sign-in">

	<h1><g:message code='spring.security.ui.login.signin'/></h1>

	<table>
		<tr>
			<td><label for="username"><g:message code='spring.security.ui.login.username'/></label></td>
			<td><input name="j_username" id="username" size="20" /></td>
		</tr>
		<tr>
			<td><label for="password"><g:message code='spring.security.ui.login.password'/></label></td>
			<td><input type="password" name="j_password" id="password" size="20" /></td>
		</tr>
		<tr>
			<td colspan='2'>
				<input type="checkbox" class="checkbox" name="${rememberMeParameter}" id="remember_me" checked="checked" />
				<label for='remember_me'><g:message code='spring.security.ui.login.rememberme'/></label> |
				<span class="forgot-link">
					<g:link controller='register' action='forgotPassword'><g:message code='spring.security.ui.login.forgotPassword'/></g:link>
				</span>
			</td>
		</tr>
	</table>

	</div>
	</form>
	</div>
  <div class="modal-footer">
    <a href="/" class="btn">Close</a>
    <a href="#" class="btn btn-primary">Save changes</a>
    <s2ui:linkButton elementId='register' controller='register' messageCode='spring.security.ui.login.register' class="btn"/>
		<s2ui:submitButton elementId='loginButton' form='loginForm' messageCode='spring.security.ui.login.login' class="btn btn-primary"/>
  </div>
</div>

<script>
$(document).ready(function() {
	$('#username').focus();
});

<s2ui:initCheckboxes/>

</script>

</body>
</html>
