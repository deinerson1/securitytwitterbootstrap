
<%@ page import="com.stb.Person" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="id" title="${message(code: 'person.id.label', default: 'Id')}" />
			
				<g:sortableColumn property="username" title="${message(code: 'person.username.label', default: 'Username')}" />
			
				<g:sortableColumn property="password" title="${message(code: 'person.password.label', default: 'Password')}" />
			
				<g:sortableColumn property="accountExpired" title="${message(code: 'person.accountExpired.label', default: 'Account Expired')}" />
			
				<g:sortableColumn property="accountLocked" title="${message(code: 'person.accountLocked.label', default: 'Account Locked')}" />
			
				<g:sortableColumn property="enabled" title="${message(code: 'person.enabled.label', default: 'Enabled')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${personInstanceList}" status="i" var="personInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${personInstance.id}">${fieldValue(bean: personInstance, field: "id")}</g:link></td>
			
				<td>${fieldValue(bean: personInstance, field: "username")}</td>
			
				<td>${fieldValue(bean: personInstance, field: "password")}</td>
			
				<td><g:formatBoolean boolean="${personInstance.accountExpired}" /></td>
			
				<td><g:formatBoolean boolean="${personInstance.accountLocked}" /></td>
			
				<td><g:formatBoolean boolean="${personInstance.enabled}" /></td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${personInstanceTotal}" />
	</div>
</section>

</body>

</html>
