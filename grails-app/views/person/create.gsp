

<%@ page import="com.stb.Person" %>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<section id="create" class="first">

	<g:hasErrors bean="${personInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${personInstance}" as="list" />
	</div>
	</g:hasErrors>
	
	<g:form action="save" class="form-horizontal" >
				
							<div class="control-group ${hasErrors(bean: personInstance, field: 'username', 'error')}">
								<label for="username" class="control-label"><g:message code="person.username.label" default="Username" /></label>
				            	<div class="controls">
									<g:textField name="username" value="${personInstance?.username}" />
									<span class="help-inline">${hasErrors(bean: personInstance, field: 'username', 'error')}</span>
								</div>
							</div>
				
							<div class="control-group ${hasErrors(bean: personInstance, field: 'password', 'error')}">
								<label for="password" class="control-label"><g:message code="person.password.label" default="Password" /></label>
				            	<div class="controls">
									<g:textField name="password" value="${personInstance?.password}" />
									<span class="help-inline">${hasErrors(bean: personInstance, field: 'password', 'error')}</span>
								</div>
							</div>
				
							<div class="control-group ${hasErrors(bean: personInstance, field: 'accountExpired', 'error')}">
								<label for="accountExpired" class="control-label"><g:message code="person.accountExpired.label" default="Account Expired" /></label>
				            	<div class="controls">
									<g:checkBox name="accountExpired" value="${personInstance?.accountExpired}" />
									<span class="help-inline">${hasErrors(bean: personInstance, field: 'accountExpired', 'error')}</span>
								</div>
							</div>
				
							<div class="control-group ${hasErrors(bean: personInstance, field: 'accountLocked', 'error')}">
								<label for="accountLocked" class="control-label"><g:message code="person.accountLocked.label" default="Account Locked" /></label>
				            	<div class="controls">
									<g:checkBox name="accountLocked" value="${personInstance?.accountLocked}" />
									<span class="help-inline">${hasErrors(bean: personInstance, field: 'accountLocked', 'error')}</span>
								</div>
							</div>
				
							<div class="control-group ${hasErrors(bean: personInstance, field: 'enabled', 'error')}">
								<label for="enabled" class="control-label"><g:message code="person.enabled.label" default="Enabled" /></label>
				            	<div class="controls">
									<g:checkBox name="enabled" value="${personInstance?.enabled}" />
									<span class="help-inline">${hasErrors(bean: personInstance, field: 'enabled', 'error')}</span>
								</div>
							</div>
				
							<div class="control-group ${hasErrors(bean: personInstance, field: 'passwordExpired', 'error')}">
								<label for="passwordExpired" class="control-label"><g:message code="person.passwordExpired.label" default="Password Expired" /></label>
				            	<div class="controls">
									<g:checkBox name="passwordExpired" value="${personInstance?.passwordExpired}" />
									<span class="help-inline">${hasErrors(bean: personInstance, field: 'passwordExpired', 'error')}</span>
								</div>
							</div>
				
		<div class="form-actions">
			<g:submitButton name="create" class="save btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
            <button class="btn" type="reset">Cancel</button>
		</div>
	</g:form>
	
</section>
		
</body>

</html>
