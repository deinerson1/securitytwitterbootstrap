import com.stb.*

class BootStrap {
  def springSecurityService

  def init = { servletContext ->
    def userRole = Authority.findByAuthority('ROLE_USER') ?: new Authority(authority:'ROLE_USER').save(failOnError:true)
    def adminRole = Authority.findByAuthority('ROLE_ADMIN') ?: new Authority(authority:'ROLE_ADMIN').save(failOnError:true)

    def adminUser = new Person(username: 'admin', enabled: true, password: 'admin1')
    adminUser.save(flush: true)
    PersonAuthority.create adminUser, adminRole, true

    def testUser = new Person(username: 'test', enabled: true, password: 'test1')
    testUser.save(flush: true)
    PersonAuthority.create testUser, userRole, true

    assert Person.count() == 2
    assert Authority.count() == 2
    assert PersonAuthority.count() == 2

    new Requestmap(url: '/authority/*', configAttribute: 'ROLE_ADMIN').save()
    new Requestmap(url: '/secure/*', configAttribute: 'ROLE_ADMIN').save()
    new Requestmap(url: '/person/*', configAttribute: 'IS_AUTHENTICATED_REMEMBERED').save()
    new Requestmap(url: '/user/*', configAttribute: 'IS_AUTHENTICATED_REMEMBERED').save()
    new Requestmap(url: '/role/*', configAttribute: 'IS_AUTHENTICATED_REMEMBERED').save()
    new Requestmap(url: '/requestmap/*', configAttribute: 'ROLE_ADMIN').save()
    new Requestmap(url: '/persistentlogin/*', configAttribute: 'ROLE_USER,ROLE_ADMIN,IS_AUTHENTICATED_FULLY').save()
    new Requestmap(url: '/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()

  }

  def destroy = {
  }

}
