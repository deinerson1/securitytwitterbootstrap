class UrlMappings {

	static mappings = {

		/*
		 * Pages without controller
		 */
//		"/"				(view:"/index")
		"/about"		(view:"/siteinfo/about")
		"/blog"			(view:"/siteinfo/blog")
		"/systeminfo"	(view:"/siteinfo/systeminfo")
		"/contact"		(view:"/siteinfo/contact")
		"/terms"		(view:"/siteinfo/terms")
		"/imprint"		(view:"/siteinfo/imprint")

		/*
		 * Pages with controller
		 * WARN: No controller should be named "api" or "mobile" or "web"!
		 */
        "/"	{
			controller	= 'home'
			action		= { 'index' }
            view		= { 'index' }
        }
        "/role/list" {
        controller	= 'authority'
			action		= { 'index' }
            view		= { 'index' }
        }
        "/user/list" {
        controller	= 'person/list'
          action		= { 'index' }
            view		= { 'index' }
        }
        "/user/create" {
        controller	= 'person/create'
          action		= { 'create' }
            view		= { 'create' }
        }
        "/requestmap/list" {
        controller	= 'requestmap'
			action		= { 'index' }
            view		= { 'index' }
        }

		"/$controller/$action?/$id?"{
			constraints {
				controller(matches:/^((?!(api|mobile|web)).*)$/)
		  	}
		}

		/*
		 * System Pages without controller
		 */
		"500"	(view:'/error')
	}
}
