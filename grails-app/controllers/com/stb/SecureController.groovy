package com.stb
import grails.plugins.springsecurity.Secured

class SecureController {

  //def springSecurityService

  //def scaffold = true
  @Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
  def index = {
    render 'Secured Access Only.'
  }
}
