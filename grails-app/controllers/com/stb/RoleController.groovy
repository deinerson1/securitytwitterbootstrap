package com.stb

class RoleController extends grails.plugins.springsecurity.ui.RoleController {
  def index = {
        redirect(action: "authority/list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [authorityInstanceList: Authority.list(params), authorityInstanceTotal: Authority.count()]
    }
}
