package com.stb

class PersistentLoginController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [persistentLoginInstanceList: PersistentLogin.list(params), persistentLoginInstanceTotal: PersistentLogin.count()]
    }

    def create = {
        def persistentLoginInstance = new PersistentLogin()
        persistentLoginInstance.properties = params
        return [persistentLoginInstance: persistentLoginInstance]
    }

    def save = {
        def persistentLoginInstance = new PersistentLogin(params)
        if (persistentLoginInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), persistentLoginInstance.id])}"
            redirect(action: "show", id: persistentLoginInstance.id)
        }
        else {
            render(view: "create", model: [persistentLoginInstance: persistentLoginInstance])
        }
    }

    def show = {
        def persistentLoginInstance = PersistentLogin.get(params.id)
        if (!persistentLoginInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
            redirect(action: "list")
        }
        else {
            [persistentLoginInstance: persistentLoginInstance]
        }
    }

    def edit = {
        def persistentLoginInstance = PersistentLogin.get(params.id)
        if (!persistentLoginInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [persistentLoginInstance: persistentLoginInstance]
        }
    }

    def update = {
        def persistentLoginInstance = PersistentLogin.get(params.id)
        if (persistentLoginInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (persistentLoginInstance.version > version) {
                    
                    persistentLoginInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'persistentLogin.label', default: 'PersistentLogin')] as Object[], "Another user has updated this PersistentLogin while you were editing")
                    render(view: "edit", model: [persistentLoginInstance: persistentLoginInstance])
                    return
                }
            }
            persistentLoginInstance.properties = params
            if (!persistentLoginInstance.hasErrors() && persistentLoginInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), persistentLoginInstance.id])}"
                redirect(action: "show", id: persistentLoginInstance.id)
            }
            else {
                render(view: "edit", model: [persistentLoginInstance: persistentLoginInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def persistentLoginInstance = PersistentLogin.get(params.id)
        if (persistentLoginInstance) {
            try {
                persistentLoginInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
            redirect(action: "list")
        }
    }
	
	
	def list_m = {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		[persistentLoginInstanceList: PersistentLogin.list(params), persistentLoginInstanceTotal: PersistentLogin.count()]
	}

	def create_m = {
		def persistentLoginInstance = new PersistentLogin()
		persistentLoginInstance.properties = params
		return [persistentLoginInstance: persistentLoginInstance]
	}

	def save_m = {
		def persistentLoginInstance = new PersistentLogin(params)
		if (persistentLoginInstance.save(flush: true)) {
			flash.message = "${message(code: 'default.created.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), persistentLoginInstance.id])}"
			redirect(action: "show_m", id: persistentLoginInstance.id)
		}
		else {
			render(view: "create_m", model: [persistentLoginInstance: persistentLoginInstance])
		}
	}

	def show_m = {
		def persistentLoginInstance = PersistentLogin.get(params.id)
		if (!persistentLoginInstance) {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
			redirect(action: "list_m")
		}
		else {
			[persistentLoginInstance: persistentLoginInstance]
		}
	}

	def edit_m = {
		def persistentLoginInstance = PersistentLogin.get(params.id)
		if (!persistentLoginInstance) {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
			redirect(action: "list_m")
		}
		else {
			return [persistentLoginInstance: persistentLoginInstance]
		}
	}

	def update_m = {
		def persistentLoginInstance = PersistentLogin.get(params.id)
		if (persistentLoginInstance) {
			if (params.version) {
				def version = params.version.toLong()
				if (persistentLoginInstance.version > version) {
					
					persistentLoginInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'persistentLogin.label', default: 'PersistentLogin')] as Object[], "Another user has updated this PersistentLogin while you were editing")
					render(view: "edit_m", model: [persistentLoginInstance: persistentLoginInstance])
					return
				}
			}
			persistentLoginInstance.properties = params
			if (!persistentLoginInstance.hasErrors() && persistentLoginInstance.save(flush: true)) {
				flash.message = "${message(code: 'default.updated.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), persistentLoginInstance.id])}"
				redirect(action: "show_m", id: persistentLoginInstance.id)
			}
			else {
				render(view: "edit_m", model: [persistentLoginInstance: persistentLoginInstance])
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
			redirect(action: "list")
		}
	}

	def delete_m = {
		def persistentLoginInstance = PersistentLogin.get(params.id)
		if (persistentLoginInstance) {
			try {
				persistentLoginInstance.delete(flush: true)
				flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
				redirect(action: "list_m")
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
				redirect(action: "show_m", id: params.id)
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'persistentLogin.label', default: 'PersistentLogin'), params.id])}"
			redirect(action: "list_m")
		}
	}

}
